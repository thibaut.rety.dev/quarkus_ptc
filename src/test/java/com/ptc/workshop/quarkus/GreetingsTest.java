package com.ptc.workshop.quarkus;

import com.ptc.workshop.quarkus.model.Person;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class GreetingsTest {

//    @Test
//    public void testHelloEndpoint() {
//        given()
//          .when().get("/helloPTC")
//          .then()
//             .statusCode(200)
//             .body(is("Hello RESTEasy"));
//    }

    private void registerUsers(String firstName, String lastName){
        final Person user = new Person(lastName, firstName);

        given()
            .body(user)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
            .when()
            .post("/helloPTC/people")
            .then()
            .statusCode(200)
            .body("firstName", is(firstName))
            .body("lastName", is(lastName));
    }

    @Test
    public void testRegisterUser() {
        registerUsers("Thibaut", "Rety");
        registerUsers("Dina", "Barsha");
        registerUsers("Ada", "Lovelace");
        registerUsers("Aurélien", "Cantor");

        given().when().get("/helloPTC/people").then().statusCode(200).body("list.size()", is(4));
    }

    @Test
    public void testGetSomeone() {
        // this test will fail
        given().when().get("/helloPTC").then().statusCode(200);
    }


}