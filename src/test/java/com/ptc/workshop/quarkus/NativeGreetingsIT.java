package com.ptc.workshop.quarkus;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeGreetingsIT extends GreetingsTest {

    // Execute the same tests but in native mode.
}