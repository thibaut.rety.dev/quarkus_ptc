package com.ptc.workshop.quarkus;

import static io.restassured.RestAssured.given;

import com.ptc.workshop.quarkus.model.Person;
import com.ptc.workshop.quarkus.service.PersonService;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import javax.inject.Inject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
public class GreetingsTestMock {
    @Inject PersonService service;

    @BeforeAll
    public static void setup() {
        PersonService mock = Mockito.mock(PersonService.class);
        Mockito.when(mock.getSomeone()).thenReturn(new Person("MockFirst", "MockLast"));
        QuarkusMock.installMockForType(mock, PersonService.class);
    }

    @Test
    public void testGetSomeone() {
        // this test will fail
        given().when().get("/helloPTC").then().statusCode(200);
    }
}
