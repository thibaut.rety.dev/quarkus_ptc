package com.ptc.workshop.quarkus;

import com.ptc.workshop.quarkus.model.Person;
import com.ptc.workshop.quarkus.service.PersonService;
import java.util.Set;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/helloPTC")
public class Greetings {

    private String userName = "PTC";
    private Person person = null;

    @Inject PersonService service;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        Person person = service.getSomeone();
        return "Hello " + person.getFirstName() + " " + person.getLastName();
    }

    // Example of a post method with queryParam
    @GET
    @Path("/query")
    @Produces(MediaType.TEXT_PLAIN)
    public String registerUserQuery(@QueryParam("userName") String userName, @QueryParam("lastName") String lastName){
        return "You are " + userName + " " + lastName;
    }

    @POST
    public Person registerUserJson(Person person){
        this.person = person;
        return person;
    }

    @POST
    @Path("/people")
    public Person registerPeople(Person person){
        this.person = person;
        service.add(person);
        return person;
    }

    @DELETE
    @Path("/people")
    public Person deletePeople(Person person){
        service.delete(person);
        return person;
    }

    @GET
    @Path("/people")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Person> getAllPeople(){
        return service.listAll();
    }


    @GET
    @Path("/json")
    public Person getUserJson(){
        return person;
    }




}