package com.ptc.workshop.quarkus.repository;

import com.ptc.workshop.quarkus.model.Person;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PersonRepository {

    private final Set<Person> people = new HashSet<>();
    public void add(Person person){
        people.add(person);
    }

    public Set<Person> listPeople(){
        return Collections.unmodifiableSet(people);
    }

    public void delete(Person person){
        people.remove(person);
    }

}
