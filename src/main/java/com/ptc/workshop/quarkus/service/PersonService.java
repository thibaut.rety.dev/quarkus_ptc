package com.ptc.workshop.quarkus.service;

import com.ptc.workshop.quarkus.model.Person;
import com.ptc.workshop.quarkus.repository.PersonRepository;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PersonService {
    private PersonRepository repository = new PersonRepository();
    public void add(Person person){
        if(person != null){
            repository.add(person);
        }
    }

    public Set<Person> listAll(){
        return repository.listPeople();
    }

    public void delete(Person person){
        repository.delete(person);
    }

    public Person getSomeone() {
        return repository.listPeople().iterator().next();
    }
}
